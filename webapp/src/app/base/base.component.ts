import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CommonService } from '../services/common.service';
import { IntentConfig } from '../model/intent-config.model';
import { FormFieldsObj } from '../model/form-fields';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html'
})
export class BaseComponent implements OnInit {

  subscriptions: Subscription[] = [];
  intentConfigData: IntentConfig;
  constructor(public commonService: CommonService) { 
  }

  ngOnInit(): void {
    
  }
  getAuthIntentObj(){
    return this.commonService.authIntentObj?.intents as string[];
  }

  getLoginFormObj(){
    return this.commonService.FormFieldsObj?.form as FormFieldsObj[];
  }

  getAssessmentFormObj(){
    return this.commonService.FormFieldsObj?.assessment as FormFieldsObj[];
  }
  
  getIntentConfig() {
    return this.commonService.intentConfigData as IntentConfig;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => {
        if (subscription) {
            subscription.unsubscribe();
        }
    });
}
}
