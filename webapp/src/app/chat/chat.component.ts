import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ChatService } from './chat.service';
import { Message } from '../model/message.model';
import { TextMessage } from '../model/text-messsage.model';
import { ResponseMessage } from '../model/response-message.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { EventManagerService } from '../services/shared-event-manager.service';//import {Aos} from 'aos';
import { BaseComponent } from '../base/base.component';
import { CommonService } from '../services/common.service';
import { AuthIntentType } from '../shared/enum/auth-intent-type.enum';
import { TranslateService } from '@ngx-translate/core';
import { FormDialogComponent } from '../rich-components/form-dialog/form-dialog.component';
import { FormFields, FormFieldsObj } from '../model/form-fields';
import { MatDialog } from '@angular/material/dialog';
import { AuthRequest } from '../model/auth-request.model';
import { LocalStorageService } from 'angular-2-local-storage';
import { EmitMessageData } from '../model/emit-message-data.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent extends BaseComponent implements OnInit {
  @ViewChild('myScrollContainer') private myScrollContainer: ElementRef;
  @Input('messages') messages: any[];
  @Input('colorBackRight') colorBackRight: string;
  @Input('colorFontRight') colorFontRight: string;
  @Input('colorBackLeft') colorBackLeft: string;
  @Input('colorFontLeft') colorFontLeft: string;
  formFieldArray: FormFieldsObj[] = [];
  RegisterReqObj: AuthRequest;
  mobileInputObj = ['RegisterMobileNumber - no', 'Register', 'resend_code'];
  textInput = '';
  openFormIntent = AuthIntentType.openFormIntent;
  messageText: string;
  authIntent: string;
  messageToBot: any;
  currentIntent: string;
  countryCode: string = '+81';
  patientRefPrefix:string = 'C000';
  mobileNumber: string;
  textYes: string[] = ["Yes", "はい"];
  summaryData: { header: string; confirmMsg: string; };
  userName: string = '';
  messageFromBot: string;
  boldText: string;
  reSendText: string[] = ["Re-send code", "コードを再送する"];
  defaultReply: any;
  updateFailResponse: string;
  redirectUrl: string;
  registeredText: string;
  apiErrorText: any;
  invalidMobileNumber: any;

  constructor(private chatService: ChatService,
    private eventManagerService: EventManagerService,
    public commonService: CommonService,
    private translate: TranslateService,
    public dialog: MatDialog,
    private localStorageService: LocalStorageService) {
    super(commonService);
  }

  ngOnInit() {
    this.subscriptions.push(this.translate.get('mec.Fallback').subscribe((text) =>
      this.defaultReply = text));
      this.subscriptions.push(this.translate.get('Fallback.UpdateFailed').subscribe((text) =>
      this.updateFailResponse = text));
      this.subscriptions.push(this.translate.get('Fallback.APIError').subscribe((text) =>
      this.apiErrorText = text));
      this.subscriptions.push(this.translate.get('Register.RegisterSuccess').subscribe((text) =>
      this.registeredText = text));
      this.subscriptions.push(this.translate.get('Fallback.InvalidPhone').subscribe((text) =>
      this.invalidMobileNumber = text));
      
    this.subscribeToExecutionEvents()
    this.messages = [];
    let messageBack: TextMessage = { "firstname": environment.firstName, "text": "Welcome onload" }
    this.subscriptions.push(this.chatService.sendMessage(messageBack)
      .subscribe((res: any) => {
        console.log(res);
        res = JSON.parse(res?.result?.replaceAll("\\\\", "")) as ResponseMessage;
        let messageReturn: Message = {
          text: res.responseMessage,
          date: new Date().toDateString(),
          userOwner: false,
          intent: res.intent
        }
        this.localStorageService.set('username', '');
        this.messages.push(messageReturn);

      }));
  }

  scrollToElement(): void {
    var container = document.getElementById("msgContainer");
    container.scrollTop = container.scrollHeight;
  }

  subscribeToExecutionEvents() {
    this.subscriptions.push(this.eventManagerService
      .getMessageToBotObservable()
      .subscribe((data: EmitMessageData) => {
        if (data?.message != null && data?.message != '' && data?.message != undefined) {
          this.boldText = data.message;
          this.subscriptions.push(this.translate.get('QuickGuide.TypeMsg').subscribe((prefix) =>
            this.messageText = prefix
          ));
          this.showSendMessageToBot(false);
        }

      }));
    this.subscriptions.push(this.eventManagerService
      .getMessageTextObservable()
      .subscribe((message: string) => {
        this.messageText = message;
        if (this.messageText != null && this.messageText != '' && this.messageText != undefined) this.handleUserAuth();
      }));
    this.subscriptions.push(this.eventManagerService
      .getCurrentIntentObservable()
      .subscribe((intent: string) => {
        this.currentIntent = intent;
      }));
    this.subscriptions.push(this.eventManagerService
      .getFormFieldArrayObservable()
      .subscribe((formObj: FormFieldsObj[]) => {
        this.formFieldArray = formObj;
      }));
    this.subscriptions.push(this.eventManagerService
      .getUserNameObservable()
      .subscribe((userName: string) => {
        if (userName) {
          this.localStorageService.set('username', userName);
          this.subscriptions.push(this.translate.get('Register.salutation').subscribe((saln) =>
            this.userName = userName.concat('  ', saln)
          ));
          
        }

      }));
    this.subscriptions.push(this.eventManagerService
      .getAuthIntentObservable()
      .subscribe((message: string) => {
        this.messageFromBot = message;
      }));
  }
  onLanguageChange(event) {
    if(event){
      console.log('langemitter');
      this.subscriptions.push(this.translate.get('mec.Fallback').subscribe((text) =>
      this.defaultReply = text));
      this.subscriptions.push(this.translate.get('Fallback.UpdateFailed').subscribe((text) =>
      this.updateFailResponse = text));
      this.subscriptions.push(this.translate.get('Register.RegisterSuccess').subscribe((text) =>
      this.registeredText = text));
      this.subscriptions.push(this.translate.get('Fallback.APIError').subscribe((text) =>
      this.apiErrorText = text));
      this.subscriptions.push(this.translate.get('Fallback.InvalidPhone').subscribe((text) =>
      this.invalidMobileNumber = text));
    }
  }

  saveTextMessage() {
    this.messageText = this.textInput;
    if (this.textInput != null && this.textInput != '' && this.textInput != undefined) this.handleUserAuth()
    if (this.mobileNumber && this.countryCode && this.mobileInputObj.includes(this.currentIntent)) {
      if(this.mobileNumber.match(/^\(?([0-9]{0,4})\)?[-. ]?([0-9]{0,4})[-. ]?([0-9]{4,9})$/)) {
        this.messageText = this.mobileNumber;
        this.handleUserAuth();
      } else {
        this.replyMessageFromBot(true, this.mobileNumber);
        this.mobileNumber = '';
        this.replyMessageFromBot(false, this.invalidMobileNumber);
      }
    }
  }

  handleUserMessage(showTextToBot: boolean = true) {
    console.log('currentIntent --' + this.currentIntent);
    if(this.currentIntent == 'UserProvidesName') this.messageText = this.patientRefPrefix.concat(this.messageText);
    this.messageToBot = null;
    this.getAuthIntentObj().forEach((item) => {
      const tempText = this.messageText.toLowerCase();
      if (tempText?.includes(item) && !this.userName) {
        this.messageToBot = 'login';
        showTextToBot = true;
        this.eventManagerService.emitAuthIntent(this.messageText);
      }
    });
    if (showTextToBot) this.showSendMessageToBot()
    this.sendTextMessageToBot();
  }

  showSendMessageToBot(isOwner = true) {
    let newMessage: Message = { text: this.messageText, date: "", userOwner: isOwner, boldText: this.boldText };
    this.messages.push(newMessage);
  }

  replyMessageFromBot(isOwner = true, message: string) {
    let newMessage: Message = { text: message, date: "", userOwner: isOwner };
    this.messages.push(newMessage);
  }

  sendTextMessageToBot() {
    let messageBack: TextMessage = { "firstname": environment.firstName, "text": this.messageToBot ?? this.messageText }
    this.subscriptions.push(this.chatService.sendMessage(messageBack)
      .subscribe((res: any) => {
        res = JSON.parse(res?.result?.replaceAll("\\\\", "")) as ResponseMessage;
        
        console.log(this.currentIntent+"==this.currentIntent ---"+res.intent);
        if (res?.intent == AuthIntentType.loginSuccuss) {
          res.responseMessage = res.responseMessage.replace(',', this.userName);
        }
        if(res?.intent == AuthIntentType.defaultFallBack){
          this.getFallbackResponse();
        }
        let messageReturn: Message = {
          text: res.responseMessage,
          date: new Date().toDateString(),
          userOwner: false,
          intent: res.intent
        }
        console.log(JSON.stringify(this.intentConfigData));
        this.eventManagerService.emitCurrentIntentObservable(res.intent);
        this.messages.push(messageReturn);
        
        if (res?.intent == AuthIntentType.loginSuccuss || res?.intent == AuthIntentType.registeredSuccussfully) {
          if (this.userName?.length > 0) {
            this.messageToBot = this.messageFromBot;
            this.sendTextMessageToBot()
          } else {
            this.showUserDetailsForm();
          }
        }
        if (res.intent == 'How_To_Use') { this.eventManagerService.emitIsChipsToBot(true); }
        else this.eventManagerService.emitIsChipsToBot(false);
      }));
    this.textInput = '';
  }

  getFallbackResponse() {
    switch(this.currentIntent) { 
      case '': { 
         //statements; 
         break; 
      } 
      case '': { 
         //statements; 
         break; 
      } 
      default: { 
         //statements; 
         break; 
      } 
   } 
  }

  getMessageFromBot(intent) {
    let messageReturn: Message = {
      text: this.messageFromBot,
      date: new Date().toDateString(),
      userOwner: true,
      intent: intent
    }
    this.messages.push(messageReturn);
    this.sendTextMessageToBot()
  }

  sendMessageToUser() {
    let messageReturn: Message = {
      text: '',
      date: new Date().toDateString(),
      userOwner: false,
      intent: this.currentIntent,
      data: { summaryData: this.summaryData, formObj: this.formFieldArray, intent: this.currentIntent }
    }
    this.eventManagerService.emitCurrentIntentObservable(AuthIntentType.saveUserInfo);
    this.messages.push(messageReturn);
  }

  onKey() {
    this.saveTextMessage();
  }
  onCountryChange(event) {
    this.countryCode = '+' + event?.dialCode;
  }

  handleUserAuth(isShowText = true) {
    this.subscriptions.push(this.translate.get('mec.Fallback').subscribe((text) =>
      this.defaultReply = text));
    const validMobileNumber = (this.mobileNumber?.length > 10) ?
    this.mobileNumber?.replace(/\D/g, '').slice(-10) : this.mobileNumber;

    if (this.mobileNumber && this.countryCode) {
      this.localStorageService.set('mobile', this.countryCode + ' ' + validMobileNumber);
    }
    if (this.currentIntent == AuthIntentType.validateMobileNumber && this.textYes?.includes(this.messageText)) {
      this.commonService.sendPassCode({ MobileNumber: validMobileNumber, CountryCode: this.countryCode }).subscribe(
        res => {
          console.log('HTTP response', res);
          if (res.status?.toLowerCase() == 'sent') this.handleUserMessage();
          else {
            this.replyMessageFromBot(false, this.apiErrorText)
            this.messageText = 'no';
            this.handleUserMessage(false);
          }
        },
        err => {
          console.log('HTTP Error', err);
          this.replyMessageFromBot(false, this.apiErrorText);
          this.messageText = 'No'; this.handleUserMessage(false);
        },

      );
     // this.handleUserMessage();
    } else if (this.currentIntent == AuthIntentType.verifySigninPasscode && !this.reSendText?.includes(this.messageText)) {
      const passcode = this.messageText;
      this.commonService.verifySigninPasscode({ MobileNumber: validMobileNumber, CountryCode: this.countryCode, Passcode: this.messageText }).subscribe(
        res => {
          console.log('HTTP response', res);
          if (res.result?.toLowerCase() == 'valid') {
            if(res?.FirstName) {
              this.assignAndEmitUserName(res);
              this.replyMessageFromBot(true, passcode);
              this.messageText = this.registeredText;
              this.handleUserMessage(false);
            } else this.handleUserMessage();
          }
          else {
            this.replyMessageFromBot(true, passcode);
            this.messageText = 'invalid';
            this.handleUserMessage(false);
          }
        },
        err => {
          console.log('HTTP Error', err);
          this.replyMessageFromBot(true, passcode)
          this.messageText = 'invalid';
          this.handleUserMessage(false);
        },

      );
    } else if (this.currentIntent == AuthIntentType.verifyPasscode ||
      this.currentIntent == AuthIntentType.reValidatePasscode) {
      const passcode = this.messageText;
      this.commonService.verifyLoginPasscode(this.messageText).subscribe(
        res => {
          console.log('HTTP response', res);
          if (res.status?.toLowerCase() == 'valid') {
            if(res.FirstName) this.assignAndEmitUserName(res);
            this.mobileNumber = res.mobileNumber;
            this.countryCode = '';
            this.localStorageService.set('mobile', this.mobileNumber);
            this.handleUserMessage();
          }
          else {
            this.replyMessageFromBot(true, passcode);
            this.messageText = 'invalid'; this.handleUserMessage(false);
          }
        },
        err => {
          this.replyMessageFromBot(true, passcode);
          console.log('HTTP Error', err); this.messageText = 'invalid'; this.handleUserMessage(false);
        },

      );
    } else if ((this.currentIntent == AuthIntentType.openFormIntent || this.currentIntent == AuthIntentType.updateUserForm)
    && !this.userName) {
      let formObj = this.getLoginFormObj();
      this.formFieldArray = formObj;
      this.openDialog();
    } else if (this.currentIntent == AuthIntentType.saveUserInfo) {
      this.updateUserInforOnConfirm(this.formFieldArray);
    }
    else this.handleUserMessage(isShowText);
  }

  showUserDetailsForm() {
    this.messageText = "update";
    this.handleUserMessage(false);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '75%',
      data: { formObj: this.formFieldArray }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.formObj) {
        this.summaryData = { header: 'Register.SummaryHeader', confirmMsg: 'Register.RegisterSuccess' };
        this.formFieldArray = result.formObj;

        this.sendMessageToUser();
      }
    });
  }

  updateUserInforOnConfirm(formObj) {
    let registerReqObj: AuthRequest = { MobileNumber: '', CountryCode: '' };
    formObj.forEach((item) => {
      
      registerReqObj.MobileNumber = (this.mobileNumber?.length > 10) ?
      this.mobileNumber?.replace(/\D/g, '').slice(-10) : this.mobileNumber;

      registerReqObj.CountryCode = this.countryCode;
      if (item.name == 'Register.FirstName') registerReqObj.FirstName = item.value;
      if (item.name == 'Register.LastName') registerReqObj.LastName = item.value;
      if (item.name == 'Register.Industry') registerReqObj.Industry = item.value;
      if (item.name == 'Register.CompanyName') registerReqObj.CompanyName = item.value;
      if (item.name == 'Register.Email') registerReqObj.Email = item.value;
    });
    this.commonService.registerUserInformation(registerReqObj).subscribe(
      res => {
        console.log('HTTP response', res);
        if (res?.FirstName) { 
          this.assignAndEmitUserName(res)
          this.handleUserMessage();
        }
      },
      err => {
        this.replyMessageFromBot(false, this.updateFailResponse);
        console.log('HTTP Error', err); this.messageText = 'invalid'; this.handleUserMessage();
      },
    );
  }

  assignAndEmitUserName(res) {
    const regex = new RegExp(/[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]/);
    var containsJapanese = (res?.FirstName?.match(regex) || res?.LastName?.match(regex));
    this.userName = containsJapanese ? res?.LastName?.concat('  ', res?.FirstName) : res?.FirstName?.concat('  ', res?.LastName);
    if(this.userName) this.eventManagerService.emitUserNameObservable(this.userName);
  }

}
