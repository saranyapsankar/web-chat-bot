import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { Message } from '../model/message.model';
import { EventManagerService } from '../services/shared-event-manager.service';
import { TranslateService } from '@ngx-translate/core'
import { lang } from 'moment';
import { ChatService } from '../chat/chat.service';
import { CommonService } from '../services/common.service';
import { FormFields, FormFieldsObj } from '../model/form-fields'
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthIntentType } from '../shared/enum/auth-intent-type.enum';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input('text') text: string;
  @Input('date') date: any;
  @Input('owner') owner: boolean;
  @Input('messageObject') messageObject: Message;
  showMedia: boolean = false;
  @Input('colorBackRight') colorBackRight: string;
  @Input('colorFontRight') colorFontRight: string;
  @Input('colorBackLeft') colorBackLeft: string;
  @Input('colorFontLeft') colorFontLeft: string;
  @Output() languageEmitter = new EventEmitter<boolean>();
  selectOptions: any[] = [
    { key: 1, name: "English", value: "en", dispName: "en-US" },
    { key: 2, name: "日本語", value: "jp", dispName: "ja-JP" }];
  assessmentOptions: any[] = [
    { key: 1, name: "assessment.Confirm" },
    { key: 2, name: "assessment.ExistingPatient" }];
  isLangSelected: boolean;
  acTypeSelected: string;
  showAssessment: boolean;
  formFieldsObj: FormFieldsObj[];
  showCreateSummary: boolean;
  showOpenFormButton: boolean;
  userName: string;
  isDisabled = false;
  unknownIntentRes: string;
  trainingSet: string;
  constructor(private eventManagerService: EventManagerService,
    private chatService: ChatService,
    private translate: TranslateService,
    public commonService: CommonService,
    private localStorageService: LocalStorageService) {
    super(commonService);
    this.userName = this.localStorageService.get('username');
    this.subscriptions.push(this.translate.get('Fallback.UnknownIntent').subscribe((text) =>
      this.unknownIntentRes = text));
    this.subscriptions.push(this.translate.get('Fallback.TrainingWords').subscribe((text) =>
      this.trainingSet = text));
  }

  ngOnInit(): void {
    console.log(this.messageObject.boldText);
    if (this.messageObject && this.messageObject.intent) {
      if (this.messageObject.intent?.includes('UserProvides') || this.messageObject.intent == 'eating_assessment' ||
        this.messageObject.intent == 'eating_assessment - existing') {
        this.showAssessment = true;
        if (this.messageObject.intent == 'UserProvidesDoesChoke - yes/no') {
          this.formFieldsObj = this.eventManagerService.formatValueInSummary(this.messageObject.text);

          let subObj = this.getAssessmentFormObj();
          const len = Object.keys(subObj).length;
          for (let i = 0; i < len; i++) {
            subObj[i]['value'] = this.formFieldsObj[i]['value'];
          }

          console.log(JSON.stringify(subObj));
          this.formFieldsObj = subObj;
        }
      } else this.showAssessment = false;
    }
    if (this.messageObject?.data) {
      this.showCreateSummary = true;
    }
    if ((this.messageObject.intent === 'RegisterMobileNumber - yes - passcode' ||
      this.messageObject.intent === 'update_user') && !this.userName) {
      this.showOpenFormButton = true;
    }
  }

  getFallbackResponse() {

  }

  onChipselect(src: string) {
    this.eventManagerService.emitMessageTextToBot(src);
  }
  onTypeChange(message: string) {
    this.subscriptions.push(this.translate.get(message).subscribe((t) =>
      this.eventManagerService.emitMessageTextToBot(t)));
  }

  onLangChange(langObj) {
    if (langObj && langObj.value) {
      this.translate.use(langObj.value);
      this.chatService.setSelectedLanguage(langObj.dispName),
        this.eventManagerService.emitMessageTextToBot(langObj.name);
      this.languageEmitter.emit(true);
      this.subscriptions.push(this.translate.get('Fallback.UnknownIntent').subscribe((text) =>
        this.unknownIntentRes = text));
      this.subscriptions.push(this.translate.get('Fallback.TrainingWords').subscribe((text) =>
        this.trainingSet = text));
    }
  }

  ngAfterViewInit() {
    var container = document.getElementById("msgContainer");
    container.scrollTop = container.scrollHeight;
  }
  onGetBookingselect() {
    window.open('http://stage-booking.livesmart.co.jp/user-calendar/', "myWindow", "width=400,height=600");
  }
}

