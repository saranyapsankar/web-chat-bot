
export interface AuthRequest {
  MobileNumber: string;
  CountryCode: string;
  Passcode?: string;
  CompanyName?: string;
  Industry?: string;
  Name?: String;
  Email?: string;
  FirstName?: String;
  LastName?: String;
}