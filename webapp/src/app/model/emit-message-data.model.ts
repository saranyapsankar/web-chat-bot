export interface EmitMessageData {
    message: string;
    isShowText: boolean;
  }