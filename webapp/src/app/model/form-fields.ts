export interface FormFields {
    name: string;
    value: string;
    options?: string [];
    required?: boolean;
  }

export interface FormFieldsObj {
    [key: number]: FormFields; //Or string instead of number
  }