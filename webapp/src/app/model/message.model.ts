export class Message{
    text?: string;
    date: string;
    userOwner: boolean;
    intent?: string;
    dataId?: string;
    data?: any;
    boldText?: string;
}