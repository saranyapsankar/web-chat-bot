import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'src/app/base/base.component';
import { FormFieldsObj } from 'src/app/model/form-fields';
import { IntentList } from 'src/app/model/intent-config.model';
import { SelectOptions } from 'src/app/model/select-options.model';
import { CommonService } from 'src/app/services/common.service';
import { EventManagerService } from 'src/app/services/shared-event-manager.service';
import { ButtonType } from 'src/app/shared/enum/button-type.enum';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() intentSelected = '';
  @Input() messageText = '';
  @Input() formFieldsObj: FormFieldsObj[] = [];
  selectOptions: SelectOptions[];
  availableOptions: SelectOptions[];
  questionText: string;
  isDropDown: boolean;
  isSuggestionChips:boolean;
  intentConfigObj: IntentList;
  summaryData = {header: 'assessment.SummaryHeader', confirmMsg: 'General.Save'};
  isDisabled = false;
  
  constructor(private formBuilder: FormBuilder,
    private eventManagerService: EventManagerService,
    private translate: TranslateService,
    public commonService: CommonService) {
   super(commonService);
  }
  assessmentForm = this.formBuilder.group({
    fieldCtrl: [null, Validators.required]
  });

  ngOnInit(): void {
    this.intentConfigObj = this.getIntentConfig().intents.find((item)=> {
      console.log(this.intentSelected+" - inside apartment list"+this.messageText);
      return item.name == this.intentSelected;
    });
    if(this.intentConfigObj && this.intentConfigObj.buttonType == ButtonType.Dropdown) {
      this.isDropDown = true;
      this.selectOptions = this.intentConfigObj.options;
      this.availableOptions = [];
    } else if(this.intentConfigObj && this.intentConfigObj.buttonType == ButtonType.SuggestionChip){
      this.isSuggestionChips = true;
      this.selectOptions = [];
      this.availableOptions = this.intentConfigObj.options;
    } else if(!this.intentConfigObj?.buttonType) {
      this.isSuggestionChips = false;
      this.isDropDown = false;
      this.selectOptions = [];
      this.availableOptions = [];
    }
  }
  onOptionChange(event: MatSelectChange) {
    if (event && event.value){
      this.subscriptions.push(this.translate.get(event.value.name).subscribe((t) =>{
        var message = (event.value.ref) ? t + ' ' +event.value.ref : t;
        this.eventManagerService.emitMessageTextToBot(message)
      }));
    }
    }
  onChipselect(message: string) { 
    this.subscriptions.push(this.translate.get(message).subscribe((text) =>
     this.eventManagerService.emitMessageTextToBot(text)));
  }
  ngAfterViewInit() {         
    this.scrollToBottom(); 
  } 

  scrollToBottom() {
    setTimeout(function(){ 
       var container = document.getElementById("msgContainer");           
       container.scrollTop = container.scrollHeight; 
     }, 1000);
  }
}
