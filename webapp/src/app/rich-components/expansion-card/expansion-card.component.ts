import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IntentList } from 'src/app/model/intent-config.model';
import { CommonService } from 'src/app/services/common.service';
import { EventManagerService } from 'src/app/services/shared-event-manager.service';

@Component({
  selector: 'app-expansion-card',
  templateUrl: './expansion-card.component.html',
  styleUrls: ['./expansion-card.component.scss']
})
export class ExpansionCardComponent extends BaseComponent implements OnInit,AfterViewInit {
  @Input() intentSelected = '';
  step = 0;
  intentConfigObj:  IntentList;
  panelOpenState: boolean = false;

  constructor(private eventManagerService: EventManagerService,
  private translate: TranslateService,
  public commonService: CommonService) {
 super(commonService);
  }

  ngOnInit(): void {
    this.intentConfigObj = this.getIntentConfig().intents.find((item)=> {
      return item.name == this.intentSelected;
    });
  }
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  onChipselect(message){
    if(message){
    this.subscriptions.push(this.translate.get(message).subscribe((text) =>
    this.eventManagerService.emitMessageToBotObservable({ message:text, isShowText :false })
    ));
    } else this.toggle(message);
    
  }

  toggle(expanded) {
   // expanded = !expanded;
 }

 ngAfterViewInit() {    
  this.scrollToBottom();      
} 

 scrollToBottom() {
  setTimeout(function(){ 
     var container = document.getElementById("msgContainer");           
     container.scrollTop = container.scrollHeight; 
   }, 300);
}
}
