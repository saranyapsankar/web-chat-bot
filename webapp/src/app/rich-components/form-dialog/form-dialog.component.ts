import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormFieldsObj } from 'src/app/model/form-fields';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.scss']
})
export class FormDialogComponent {
  dynamicForm: FormArray;
  modalFormGroup: FormGroup;
  formFieldArray: FormFieldsObj[];
  isSubmitClicked = false;
  public myForm: FormGroup = this.fb.group({});
  mobileNumber: string;
  constructor(private fb: FormBuilder,
    private localStorageService: LocalStorageService,
    public dialogRef: MatDialogRef<FormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.mobileNumber = this.localStorageService.get('mobile');
    }

    ngOnInit() {
      const fieldArray = this.data?.formObj;
      
      for (const control of fieldArray) {
        control.value = (control.name == 'Register.Mobile') ? this.mobileNumber: control.value;
        const validator = (control.required) ? Validators.required: null;
        this.myForm.addControl(
          control.name,
          this.fb.control(control.value, validator)
        );
      }
      
    }

  onNoClick(): void {
    if(this.myForm.valid) {
    this.dialogRef.close({
      formObj: this.formatFormGroupObject() ?? ''
    });
    
  } else {
    this.isSubmitClicked = true;
  }
  }

  safeClose(){
    if (!this.myForm.touched && !this.myForm.dirty) {
      this.dialogRef.close();
      return;
    } else if(this.myForm.invalid){ this.isSubmitClicked = true; }
  }

  formatFormGroupObject(){
    this.data?.formObj.forEach((item) => {
      item.value = this.myForm?.value[item.name];
   })
   console.log(JSON.stringify(this.data?.formObj));
    return this.data?.formObj;
  }
}
