import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'src/app/base/base.component';
import { FormFieldsObj } from 'src/app/model/form-fields';
import { CommonService } from 'src/app/services/common.service';
import { EventManagerService } from 'src/app/services/shared-event-manager.service';
import { FormDialogComponent } from '../form-dialog/form-dialog.component';

@Component({
  selector: 'app-summary-card',
  templateUrl: './summary-card.component.html',
  styleUrls: ['./summary-card.component.scss']
})
export class SummaryCardComponent extends BaseComponent implements OnInit {
  @Input() messageText = '';
  @Input() formFieldArray: FormFieldsObj[] = [];
  @Input() summaryData: {header:'', confirmMsg:''};
  constructor(
    private eventManagerService: EventManagerService,
    private translate: TranslateService,
    public commonService: CommonService,
    public dialog: MatDialog) {
    super(commonService);
  }

  title = 'angular-material-modals';

  city: string;
  name: string;
  food_from_modal: string;
  ngOnInit(): void {
    console.log(this.messageText);
    this.subscribeToExecutionEvents();
  }

  subscribeToExecutionEvents() {
    this.subscriptions.push(this.eventManagerService
      .getFormFieldArrayObservable()
      .subscribe((data: FormFieldsObj[]) => {
        console.log('emitting data' + data);
        if (data) this.formFieldArray = data;
      }));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(FormDialogComponent, {
      width: '75%',
      data: { formObj: this.formFieldArray }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(JSON.stringify(result));
      if(result?.formObj)
      this.formFieldArray = result.formObj;
      this.eventManagerService.emitFormFieldArray(this.formFieldArray);
    });
  }

confirmSummary(message: string) {
    this.subscriptions.push(this.translate.get(message).subscribe((text) =>
     this.eventManagerService.emitMessageTextToBot(text)));
  }

}
