import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpServiceService {
  constructor(private http: HttpClient) { }

  apiUrl = environment.backend.apiUrl;
  
  setHeaders(): HttpHeaders {
    const headersConf = {};
    const token = environment.token
    headersConf['Access-Control-Allow-Origin'] = '*';
    headersConf['Content-Type'] = 'application/json; charset=utf-8';
    if (token) {
      // @ts-ignore
      headersConf.Authorization = token;
    }
    return new HttpHeaders(headersConf);
  }

  get(path: string): Observable<any> {
    const headers = this.setHeaders();
    return this.http.get(this.apiUrl + path, { headers });
  }

  post(path: string, body, responseType?: string): Observable<any> {
    const headers = this.setHeaders();
    const options: any = { headers };
    if (responseType === 'text') {
      options.responseType = 'text';
    }
    return this.http.post(this.apiUrl + path, body, options);
  }
  
}
