import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiHttpServiceService } from './api-http-service.service';
import { AuthRequest } from '../model/auth-request.model';
import { FormFieldsObj } from '../model/form-fields';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  intentConfigData: any;
  authIntentObj: any;
  FormFieldsObj: any;
  constructor(private http: HttpClient,
    private httpService: ApiHttpServiceService) {
    this.getIntentConfigJSON().subscribe(data => {
      this.intentConfigData = data;
    });
    this.getAuthIntentJSON().subscribe(data => {
      this.authIntentObj = data;
    });
    this.getLoginFormJSON().subscribe(data => {
      this.FormFieldsObj = data;
    });
  }

  public getIntentConfigJSON(): Observable<any> {
    if (this.intentConfigData) return this.intentConfigData;
    return this.http.get("assets/config/intent-config.json");
  }

  public getAuthIntentJSON(): Observable<any> {
    if (this.authIntentObj) return this.authIntentObj;
    return this.http.get("assets/config/login-intents.json");
  }

  public getLoginFormJSON(): Observable<any> {
    if (this.FormFieldsObj) return this.FormFieldsObj;
    return this.http.get("assets/config/register-form.json");
  }

  public sendPassCode(data: AuthRequest): Observable<any> {
    return this.httpService.post('SendPassCcode', data);
  }

  public verifySigninPasscode(data: AuthRequest): Observable<any> {
    return this.httpService.post('ValidatePasscode', data);
  }

  public verifyLoginPasscode(queryParam: string): Observable<any> {
    return this.httpService.get('VerifyPassCode/?passcode=' + encodeURIComponent(queryParam));
  }
  public getBooklyRedirectUrl(queryParam: string): Observable<any> {
    return this.httpService.get('GetBooklyRedirectUrl?mobileNumber=' + encodeURIComponent(queryParam));
  }
  //GetBooklyRedirectUrl?mobileNumber=9745253961
  public registerUserInformation(data: AuthRequest): Observable<any> {
    return this.httpService.post('UpdateRegisterUserInfo', data);
  }

}
