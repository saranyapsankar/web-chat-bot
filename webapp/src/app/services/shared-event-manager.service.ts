import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { EmitMessageData } from '../model/emit-message-data.model';
import { FormFieldsObj } from '../model/form-fields';
@Injectable({
    providedIn: 'root'
})
export class EventManagerService {
    private messageTextSubject = new Subject<string>();
    private messageText$: Observable<string> = this.messageTextSubject.asObservable();
    private isChipsSubject = new Subject<boolean>();
    private isChips$: Observable<boolean> = this.isChipsSubject.asObservable();
    private acTypeSelectedSubject = new Subject<string>();
    private acTypeSelected$: Observable<string> = this.acTypeSelectedSubject.asObservable();
    private appointmentCardSubject = new Subject<string>();
    private appointmentCard$: Observable<string> = this.appointmentCardSubject.asObservable();

    private authIntentSubject = new Subject<string>();
    private authIntent$: Observable<string> = this.authIntentSubject.asObservable();

    private formFieldArraySubject = new Subject<FormFieldsObj[]>();
    private formFieldArray$: Observable<FormFieldsObj[]> = this.formFieldArraySubject.asObservable();
    private currentIntentSubject = new Subject<string>();
    private currentIntent$: Observable<string> = this.currentIntentSubject.asObservable();
    private userNameSubject = new Subject<string>();
    private userName$: Observable<string> = this.userNameSubject.asObservable();
    private messageToBotSubject = new Subject<EmitMessageData>();
    private messageToBot$: Observable<EmitMessageData> = this.messageToBotSubject.asObservable();
    
    constructor() {
    }

    emitMessageToBotObservable(value: EmitMessageData) {
        this.messageToBotSubject.next(value);
    }
    getMessageToBotObservable(): Observable<EmitMessageData> {
        return this.messageToBot$;
    }

    emitCurrentIntentObservable(value: string) {
        this.currentIntentSubject.next(value);
    }
    getCurrentIntentObservable(): Observable<string> {
        return this.currentIntent$;
    }
    emitUserNameObservable(value: string) {
        this.userNameSubject.next(value);
    }
    getUserNameObservable(): Observable<string> {
        return this.userName$;
    }
    emitMessageTextToBot(value: string) {
        this.messageTextSubject.next(value);
    }
    getMessageTextObservable(): Observable<string> {
        return this.messageText$;
    }
    emitIsChipsToBot(value: boolean) {
        this.isChipsSubject.next(value);
    }
    getIsChipsObservable(): Observable<boolean> {
        return this.isChips$;
    }
    emitAcTypeSelected(value: string) {
        this.acTypeSelectedSubject.next(value);
    }
    getAcTypeSelectedObservable(): Observable<string> {
        return this.acTypeSelected$;
    }

    emitAppointmentCard(value: string) {
        this.appointmentCardSubject.next(value);
    }
    getAppointmentCardObservable(): Observable<string> {
        return this.appointmentCard$;
    }
    emitAuthIntent(value: string) {
        this.authIntentSubject.next(value);
    }
    getAuthIntentObservable(): Observable<string> {
        return this.authIntent$;
    }

    emitFormFieldArray(value: FormFieldsObj[]) {
        this.formFieldArraySubject.next(value)
    }
    getFormFieldArrayObservable(): Observable<FormFieldsObj[]> {
        return this.formFieldArray$;
    }

    formatValueInSummary(str: string): FormFieldsObj[] {
        const tempFieldObj = str.split(/\r?\n/).filter(item => item?.includes(':'));
        const trial = tempFieldObj.map(pair => pair.split(": "));
        return trial.map(([name, value]) => ({ name, value }));
    }

}
