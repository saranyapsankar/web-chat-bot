export enum AuthIntentType{
    verifyPasscode = "check_login - yes",
    validateMobileNumber = "RegisterMobileNumber",
    verifySigninPasscode = "RegisterMobileNumber - yes",
    register = "Register",
    reValidatePasscode = "login_passcode_reenter",
    openFormIntent = "RegisterMobileNumber - yes - passcode",
    saveUserInfo = "Save_User_Info",
    registeredSuccussfully = "registered_successfully",
    loginSuccuss = "login_success",
    updateUserForm = "update_user",
    defaultFallBack = "Default Fallback Intent"
}