// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  firstName: 'MEC',
  dialogflow: {
    projectId: "web-bot-multilingual-dbqx"
  }, 
  token: 'Bearer fZZETUsTObR0W/Ubq+jG4axhE6rOQCOu8TK88f24CD8fCWMtqjvXeW/ulv/Q/oG4JKLN0MKsVSr2H4gJatQzw166Fz3dIGyi2jDouHVHJ28NH3HUKZoAUZjg2/aD+HH9znMr2PaJX2wQY9WSqYltRQdB04t89/1O/w1cDnyilFU=',
  backend: {
     requestTextUrl: "http://localhost:52150/LiveSmart/ConnectWithDialogFlowAPI",
     apiUrl: "http://localhost:52150/InteractiveWebBOT/" 
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
